# Install Gentoo Wiki Grey

## About
A a grey theme for wiki.installgentoo.com with a mascot. The mascot is Gentoo from Kemono Friends. She is base64 encoded, so this theme does not use any third party files. The font is monospace with pixel size and is perfect for using your favorite bitmap system font on.

## Links
Userstyles.org page: https://userstyles.org/styles/149132/install-gentoo-wiki-grey

## Screenshot
![homepage](igw-theme.png)